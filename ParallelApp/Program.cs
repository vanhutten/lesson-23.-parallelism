﻿using System;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;

namespace ParallelApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr100 = new int[100000];
            int[] arr1000 = new int[1000000];
            int[] arr10000 = new int[10000000];
            SetArray(arr100);
            SetArray(arr1000);
            SetArray(arr10000);
            
            var timer1 = DateTime.Now;
            Console.WriteLine($"Последовательное выполнение сумма 100000 эл   = {CalcSum(arr100)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");
            timer1 = DateTime.Now;
            Console.WriteLine($"Последовательное выполнение сумма 1000000 эл  = {CalcSum(arr1000)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");
            timer1 = DateTime.Now;
            Console.WriteLine($"Последовательное выполнение сумма 10000000 эл = {CalcSum(arr10000)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");          
            
           // ThreadPool.SetMinThreads(1,8);
            
            timer1 = DateTime.Now;
            Console.WriteLine($"Паралелльное выполнение сумма 100000 эл = {CalcParallelSum(arr100,8)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");

            timer1 = DateTime.Now;
            Console.WriteLine($"Паралелльное выполнение сумма 1000000 эл = {CalcParallelSum(arr1000, 8)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");

            timer1 = DateTime.Now;
            Console.WriteLine($"Паралелльное выполнение сумма 10000000 эл = {CalcParallelSum(arr10000, 8)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");

            timer1 = DateTime.Now;
            Console.WriteLine($"LINQ Parallel выполнение сумма 100000 эл   = {ParallelSumLinq(arr100)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");

            timer1 = DateTime.Now;
            Console.WriteLine($"LINQ Parallel выполнение сумма 1000000 эл  = {ParallelSumLinq(arr1000)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");

            timer1 = DateTime.Now;
            Console.WriteLine($"LINQ Parallel выполнение сумма 10000000 эл = {ParallelSumLinq(arr10000)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс");

           // Task task = new Task(()=> Console.WriteLine($"Выполнение через Task сумма 100000 эл = {CalcSum(arr100)} время - {(DateTime.Now - timer1).TotalMilliseconds} мс"));
          //  task.Start();
            


            Console.ReadLine();
        }

        public static long CalcSum(int[] array)
        {
            long sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum+=array[i];
            }
            return sum;
        }

        public static void SetArray(int[] array)
        {
            var rnd = new Random();            
            for (int i=0;i<array.Length;i++)
            {
                array[i] = rnd.Next(100);
            }
        }

        public static long CalcPartialSum(int[] arr, int startindex, int endindex)
        {
            long sum = 0;
            for (int i = startindex; i <= endindex; i++)
            {
                sum += arr[i];
            }
            return sum;
        }

        public static long CalcParallelSum(int[] arr, int ThreadsNumber)
        {
            long sum = 0;
            int starti;
            int chunk = arr.Length / ThreadsNumber;
            int endi;
            for (int i=0;i<ThreadsNumber;i++)
            {
                starti = i * chunk;
                endi = i + 1 == ThreadsNumber ? arr.Length-1 : chunk *(i+1) - 1;
                var result = Task.Run(()=> CalcPartialSum(arr, starti, endi));
                sum += result.Result;
            }
            
            return sum;
        }

        public static long ParallelSumLinq(int[] arr)
        {
            long sum = arr.AsParallel().Sum();
            return sum;
        }
    }
}
